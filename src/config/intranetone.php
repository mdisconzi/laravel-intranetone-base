<?php
/*
|--------------------------------------------------------------------------
| Dataview IntranetOne Configs
|--------------------------------------------------------------------------
*/

return [
    'client' =>[
      "name"=>"Veneza Plaza Hotel",
      "address"=>"Av. Pará, nº 1823",
      "address2"=>"Centro",
      "full_address"=>"Av. Pará, nº 1823, centro, Gurupi-TO",
      "zipcode"=>"77403-010",
      "city"=>"Gurupi",
      "state"=>"TO",
      "phone"=>"63 3312-3500",
      "mobile"=>"63 98456-6547",
      "whatsapp"=>"63 98456-6547",
      "email"=>"veneza.reserva@gmail.com",
      "email2"=>"",
      "cnpj"=>""
    ],
    'path_storage' => 'app/public/intranetone/',
      'social_media'=>[
        'facebook'=>[
          'app_id'=>'175292446393043',
          'app_version'=>'v2.11',
          'long_token' =>'EAACfbXPZCwtMBADtXWL4qg2JWnIJ0Mk6n4Y04cviYZA0mvWY5NPJWwsosqvekwZAyGAS96Mk4i1AWORAZCpPdICyuxZAGAAAormlRHTIzfpIKp2wTqeRsFi6z2VR3YN8oioCwPVJP29WslvuxkxFDZBAMqQfJCHg5TLSX0ZAuwJpgZDZD',
          "locale"=>"pt_BR",
          'publisher'=>""//o correto é este dado ser vinculado de alguma forma ao usuário que postou
        ],
        'twitter'=>[
          'user'=>'',
          'publisher'=>""//o correto é este dado ser vinculado de alguma forma ao usuário que postou
        ],
        'google'=>[
          'UA'=>'UA-107257069-1'
        ]
      ]
        ];

